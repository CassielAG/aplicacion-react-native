import React from 'react'
import { StyleSheet, View, Button } from 'react-native'

export default function BtnOfuscar(props) {
    function ofuscar() {
        
        let resultado = props.info.split('');
        let palabra = resultado.length;
        let c=1;
        
        for (let i = 0; i < palabra; i++) {
            resultado.splice(i+c,0,"1");
            c++;
        }
        alert("Mensaje ofuscado: " + resultado.join(''));
    }

    return (
        <View>
            <Button
                onPress={()=> ofuscar()}
                title="Ofuscar"
                color="#841524"
            />
        </View>
    )
}

const styles = StyleSheet.create({})


//Autor: Agüero Gloria José Cassiel