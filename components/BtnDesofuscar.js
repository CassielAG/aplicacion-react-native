import React from 'react'
import { StyleSheet, View, Button } from 'react-native'

export default function BtnDesofuscar(props) {
    function desofuscar() {
        let resultado = props.info.split('');
        let palabra = resultado.length;
        
        for (let i = 0; i < palabra; i++) {
            if (resultado[i]=="1") {
                resultado.splice(i,1);
            }
        }

        alert("Mensaje ofuscado: " + resultado.join(''));
    }

    return (
        <View style={styles.buttons}>
            <Button 
                onPress={() => desofuscar()}
                title="Desofuscar"
                color="#261584"
            />
        </View>
    )
}

const styles = StyleSheet.create({
  buttons: {
    margin: 15,
  },
});

//Autor: Agüero Gloria José Cassiel
