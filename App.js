import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import BtnOfuscar from './components/BtnOfuscar';
import BtnDesofuscar from './components/BtnDesofuscar'

export default function App() {
const [palabra, setpalabra] = useState("")

  return (
    <View style={styles.container}>
      <Text></Text>
      <TextInput
        style={styles.input}
        placeholder="Ingresa una palabra"
        onChange = {(e) => {
          setpalabra(e.nativeEvent.text);
        }}
      />
      <BtnOfuscar style={styles.buttons} info={palabra}/>
      <BtnDesofuscar style={styles.buttons} info={palabra}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12
  }
});


//Autor: Agüero Gloria José Cassiel